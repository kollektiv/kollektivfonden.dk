---
title: "Kollektivfonden*"
type: "static"
---

En fælles fond, der samler eksisterende kollektivers lån og støtter opstarten af nye kollektiver. 

\* Kollektivfonden eksisterer endnu ikke, men initiativgruppen for en kollektivfond
arbejder frem imod at starte den op i løbet af 2019. Følg med på [kollektivfonden.dk](https://kollektivfonden.dk)


## Hvorfor en fond?

Ved at samle kollektivers lån og bolig i en fælles fond vil vi:

### 1. Sikre de allerede eksisterende kollektivers fortsatte beståen som kollektiver.
Mange af de nuværende kollektiver eksisterer i dag fordi de på solidarisk vis er gået i arv fra de personer som har startet dem, og har valgt at give dem videre uden økonomisk vinding. Ved at koble kollektiverne sammen i en fond, sikres det at de for fremtiden også består. 


### 2. Etablere nye kollektiver 

Grupper der gerne vil starte et nyt kollektiv kan ansøge fonden om køb af ny bolig F.eks.:
* Kollektiver vil gerne købe en lejlighed mere i opgangen
* Udbrydergruppe vil starte nyt kollektiv


### 3. Gøre det økonomisk bæredygtigt at bo i kollektiv 

At bo i kollektiv skal være for alle uanset økonomiske ressourcer. Ved at samle eksisterende kollektivers lån kan der opnåes bedre lån og dermed billigere husleje for de kollektiver der er en del af fonden







