---
title: "KOLLEKTIVFONDENS MANIFEST - UDKAST*"
draft: false
---

*København, 2018*

*Kollektivfonden er en respons på nødvendigheden og relevansen af kollektivernes fortsatte beståen og udbredelse i dagens Danmark. Kollektivfonden står på skuldrene af kollektivbevægelsen i Danmark fra slutningen af 1960’erne, som var visionær i sine forestillinger om, hvordan vi kan bo sammen. Flere af de samme mærkesager som samlede bevægelsen dengang er stadig relevante i dag og kan på mange måder også bidrage til at løse flere af de problematikker, som vi i dag står overfor som samfund og individ.*

*Kollektivfonden tror på, at fællesskaber næres i kollektiver. Fællesskaber styrker solidariteten og solidaritet kan modvirke ensomhed, egoisme og samfundets atomisering. Kollektivet er i sig selv meningsfuldt uden at være et middel til et større mål, men Kollektivet sætter også en retning mod et mere socialt, bæredygtigt, inkluderende og empatisk samfund.*

*Mange af de nuværende kollektiver eksisterer i dag fordi de på solidarisk vis er gået i arv fra de personer som har startet dem, og har valgt at give dem videre uden økonomisk vinding. Ved at koble kollektiverne sammen i en fond, sikres det at de også i fremtiden består.*

Kollektivfondens formål er i prioriteret rækkefølge: 

1. Sikre de allerede eksisterende kollektivers fortsatte beståen som kollektiver. 
1. Etablere nye kollektiver
1. Gøre det økonomisk bæredygtigt at bo i kollektiv 

I det følgende uddybes det ideologiske grundlag for Kollektivfondens formål, og for prioriteringen af formålene.

## 1) Sikre de allerede eksisterende kollektivers fortsatte beståen som kollektiver. 

### Sikring af kollektivernes beståen

Mange kollektiver er i de seneste år blevet afviklet af økonomiske årsager. Kollektivfonden ønsker derfor, at eksisterende kollektiver solidarisk forpligter sig til fortsat at eksistere som kollektiver, ved at fjerne muligheden for salg af den kollektive ejendom for individuel økonomisk vinding, og i samme greb hjælpe til økonomisk i kollektiver med uforløste potentialer. 

Mange af de nuværende kollektiver eksisterer i dag, fordi de på solidarisk vis er gået i arv fra de personer som har startet dem og har valgt at give dem videre uden økonomisk vinding. Ved at koble kollektiverne sammen i en fælles fond, sikres det at de for fremtiden også består. 

### Kollektivernes selvbestemmelse

Kollektivet rummer mange variationer og grader af fællesskab, sammensætning og organisering. Et kollektiv kan favne enkeltpersoner såvel som par og familier; unge såvel som gamle. Central er dog idéen om, at fællesskabet styrker den enkelte beboer. 

Kollektivet defineres af dem, som bor i kollektivet. Kollektiverne kan fungere autonomt, men skal leve op til Kollektivfondens definition af et kollektiv. Kollektivfonden har ikke til hensigt at udstikke retninger for hvordan de enkelte kollektiver skal fungere. En høj grad af ejerskab af ønskes bevaret, da ejerskab blandt kollektivister er med til at sikre et kollektivs beståen.

### Tættere samarbejde mellem kollektiverne

Kollektivfonden ønsker at skabe et tættere sammenhold mellem både eksisterende og nye kollektiver. Kollektiverne har meget at lære af hinandens måder at drive kollektiv på, og en erfaringsdeling kollektiverne imellem kan styrke de enkelte kollektivers organisering og dermed sikre deres beståen. 

### Stabilitet for alle frem for vinding for få

Et kollektiv har ikke profit for øje, men kan stadig have øje for profit. Kollektivfonden ønsker ikke, at man skal kunne spekulere i økonomiske gevinster ved at opløse kollektiver. Økonomisk stabilitet kan være befordrende for fællesskabet. Derfor ønsker Kollektivfonden at ingen enkeltperson kan have udsigt til gevinst ved et eventuelt salg. 


## 2) Etablere nye kollektiver 

Der eksisterer ikke kollektiver nok i Danmark til at imødekommende den store efterspørgsel. Samtidig er det meget vanskeligt at starte nye kollektiver op i byerne pga. boligpriserne, som er støt stigende. Kollektivfonden ønsker, at den store efterspørgsel imødekommes, så folk får mulighed for at bo, som de ønsker.

### Fællesskab frem for ensomhed

I Kollektivfondens forståelse af et kollektiv er man som kollektivist i daglig kontakt med sine bofæller. Kollektivfonden ønsker at fremme tilstedeværelsen af kollektiver, som modvirker den ensomhed som hyppigere og hyppigere ses i Danmark. I et kollektiv nærer følelsen af meningsfuldhed og lykke nærer den enkelte, og dermed også kommer de andre til gode.

### Grønne fordele

Kollektivfonden ønsker at etablere nye kollektive boliger, fordi det at bo sammen i fællesskaber er mere bæredygtigt for miljøet, end hvis vi bor alene eller få sammen. Ved at deles om alt fra vaskemaskine, avisabonnement til vand og varme, sparer vi sammen på ressourcerne i en tid hvor vi alle har et fælles ansvar for at værne om planeten.

### Tilbagetage ejerforhold

Kollektivfonden ønsker at tilbagetage ejerforholdene og kontrollen over vores boligforhold. Ejerskabet skal tilbage på de manges hænder snarere end de få. Det giver frihed og selvbestemmelse, frem for at skulle underlægge sig boligmarkedets logik og udlejers vilkår.

### Frigørelse af tid og selvrealisering

At bo i kollektiv frigør tid for det enkelte individ da man i et kollektiv deles om de huslige opgaver som madlavning, rengøring, vedligehold, daglige indkøb og lign. Frigørelsen af tid kan på den ene side hjælpe os til at undgå stress som flere og flere diagnosticeres med i dag. Derudover kan frigørelsen af tid give kollektivisterne mulighed til at realisere sig selv i større grad og medføre menneskelig opblomstring. 

### Empati, solidaritet og sameksistens

At bo i kollektiv kan være én måde at styrke evnen hos kollektivisterne til at opnå større gensidig forståelse, konstruktiv kommunikation og fredelig sameksistens. Evner som også er gavnlige i andre af livets forhold uden for kollektivet. 

Ved at bo i kollektiv møder man ofte nye mennesker, og man kan blive udfordret i sin verdensanskuelse og opnå større forståelse for andre menneskers verdenssyn og livsførelse. Ved at bo i kollektiv lærer den enkelte at relatere sine egne behov over for fællesskabets behov. En læring, som både styrker kollektivet og som kan diffundere ud i andre samfundsforhold.

### Ind i byen - ud på landet

Flere og flere flytter til byerne. Ved at bo kollektivt deles vi også kvadratmeterne, og flere mennesker får mulighed for at bo centralt. Samtidig ses også en bevægelse ud af byerne. Flere opsøger livet på landet, hvor der også oprettes kollektiver. Kollektivfonden ønsker også at imødekomme og støtte begge bevægelser. 


## 3) Gøre det økonomisk bæredygtigt at bo i kollektiv

### Kollektiv for alle

At bo i kollektiv skal være for alle og ikke afhængigt af økonomiske ressourcer. Kollektivfonden ønsker derfor at gøre det økonomisk muligt for alle at bo i kollektiv. Ved at bo i kollektiv deles man om husleje og andre udgifter forbundet med det at bo, og der er derfor oftere lavere boligudgifter for den enkelte ved at bo kollektivt i stedet for individuelt. 

### Fordelene ved en fond

Kollektivfonden ønsker at gøre det økonomisk attraktivt at bo i kollektiv og derfor skal omkostningerne ikke overstige omkostningerne ved at bo alene. Ved at sammenbinde mange kollektivers økonomi, kan der opnås fordele i forhold til optagelse af billige lån, og for nyopstartede kollektiver, som ellers ikke ville have samme muligheder.


\* *Kollektivfonden eksisterer endnu ikke, men initiativgruppen for en kollektivfond arbejder frem imod at starte den op i løbet af 2019. Følg med på [kollektivfonden.dk](https://kollektivfonden.dk).*

